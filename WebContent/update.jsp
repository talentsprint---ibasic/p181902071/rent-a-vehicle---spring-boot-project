<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

		<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/welcome" class="navbar-brand"> <text
					style="width:40px;font-family: verdana; color : red;">Rent<i
					style="width: 50px; color: yellow">A<sup>awesome</sup></i> <b
					style="width: 40px; color: blue">Vehicle</b></text>
			</a>

			<div class="navbar-collapse collapse" style="float: right">
				<ul class="nav navbar-nav">
					
						<c:if test="${pageContext.request.userPrincipal.name != null}">
							<form id="logoutForm" method="POST"
								action="${contextPath}/logout">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>

							<h2>
								Welcome ${pageContext.request.userPrincipal.name} | <a
									onclick="document.forms['logoutForm'].submit()">Logout</a>
							</h2>
						</c:if>
						
					
				</ul>
			</div>
		</div>
	</div>

	<div class="container text-center">
		<h3> ${bike.subtypeid.typename} is ${b.status}</h3>
		<hr>
		


			<h3 style="font-family: verdana;">Mail has been sent to
				${bike.ownerid.first_name }</h3>

			<form>
  <input type="button" value="Go Back to Your Posts" onclick="history.go(-1)" class = "btn btn-success">
</form>

		

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>

</body>
</html>