<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

		<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/welcome" class="navbar-brand"> <text
					style="width:40px;font-family: verdana; color : red;">Rent<i
					style="width: 50px; color: yellow">A<sup>awesome</sup></i> <b
					style="width: 40px; color: blue">Vehicle</b></text>
			</a>

			<div class="navbar-collapse collapse" style="float: right">
				<ul class="nav navbar-nav">
					
						<c:if test="${pageContext.request.userPrincipal.name != null}">
							<form id="logoutForm" method="POST"
								action="${contextPath}/logout">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>

							<h2>
								Welcome ${pageContext.request.userPrincipal.name} | <a
									onclick="document.forms['logoutForm'].submit()">Logout</a>
							</h2>
						</c:if>
						
					
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<c:choose>
			<c:when test="${mode=='CARPOST'}">
				<spring:url value="/addCar" var="saveURL" />

				<h2>Car Details to fill</h2>
				<form:form modelAttribute="postad" method="post"
					action="${saveURL }" cssClass="form">
					<form:hidden path="id" />
					<div class="form-group">
						<label>Carname</label>
						<form:input path="carname" cssClass="form-control" id="carname" />
					</div>
						<div class="form-group">
						<form:hidden path="subtypeid" cssClass="form-control"
							id="subtypeid.subid" />
					</div>
					<div class="form-group">
						<form:hidden path="localityid" cssClass="form-control"
							id="localityid.localityid" />
					</div>
					<div class="form-group">
						<form:hidden path="ownerid" cssClass="form-control" id="ownerid.id" />
					</div>
					<div class="form-group">
						<label>Year Of Purchase </label>
						<form:input path="YOP" cssClass="form-control" id="YOP" />
					</div>
					
					<div class="form-group">
						<label>Colour</label>
						<form:input path="colour" cssClass="form-control" id="colour" />
					</div>
					
					<div class="form-group">
						 <label>Fuel</label>
						&nbsp;&nbsp;&nbsp;
							<input type="radio" name="fuel" value="Petrol" checked>Petrol&nbsp;&nbsp;&nbsp;
							<input type="radio" name="fuel" value="Diesel">Diesel
					</div>
					<div class="form-group">
						<label>Image</label>
						<form:input path="image" cssClass="form-control" id="image" />
					</div>
					
					<div class="form-group">
					<h4>
						<label>Seating</label>&nbsp;&nbsp;&nbsp;
						<input type="radio" name="seating" value="2" checked>2&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="4">4&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="3">5&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="6">6</h4>
					</div>
					<div class="form-group">
						<h4><label>Transmission</label>&nbsp;&nbsp;&nbsp;
						
							<input type="radio" name="transmission" value="Manual" checked>Manual&nbsp;&nbsp;&nbsp;
							<input type="radio" name="transmission" value="Automatic">Automatic</h4>
					</div>
					<div class="form-group">
						<label>Amount per day</label>
						<form:input path="amount" cssClass="form-control" id="amount" />
					</div>
					<div class="form-group">
					<h4>
						     <label>Status</label>
						&nbsp;&nbsp;&nbsp;
							<input type="radio" name="status" value="Available" checked>Available&nbsp;&nbsp;&nbsp;
							<input type="radio" name="status" value="Not Available">Not
							Available
						</h4>

					</div>

					<button type="submit" class="btn btn-primary">
						Save
						

					</button>
				</form:form>
			</c:when>
			<c:when test="${mode=='BIKEPOST'}">
				<spring:url value="/saveBike" var="saveURL" />

				<h2>BIKE Details to fill</h2>

			<h4>	<form:form modelAttribute="postad" method="post"
					action="${saveURL }" cssClass="form">
					<form:hidden path="bikeid" />
					<div class="form-group">
						<label>Bike name</label>
						<form:input path="bikename" cssClass="form-control" id="bikename" />
					</div>
					<div class="form-group">
						<form:hidden path="subtypeid" cssClass="form-control"
							id="subtypeid.subid" />
					</div>
					<div class="form-group">
						<form:hidden path="localityid" cssClass="form-control"
							id="localityid.localityid" />
					</div>
					<div class="form-group">
						<form:hidden path="ownerid" cssClass="form-control" id="ownerid.id" />
					</div>
					<div class="form-group">
						<label>Year Of Purchase </label>
						<form:input path="YOP" cssClass="form-control" id="YOP" />
					</div>
					<div class="form-group">
						<label>BHP</label>
						<form:input path="bhp" cssClass="form-control" id="bhp" />
					</div>
					<div class="form-group">
						<label>Colour</label>
						<form:input path="colour" cssClass="form-control" id="colour" />
					</div>
					<div class="form-group">
						<label>Displacement</label>
						<form:input path="displacement" cssClass="form-control"
							id="displacement" />
					</div>
					<div class="form-group">
						<label>Fuel</label>
						&nbsp;&nbsp;&nbsp;
							<input type="radio" name="fuel" value="Petrol" checked>Petrol&nbsp;&nbsp;&nbsp;
							<input type="radio" name="fuel" value="Diesel">Diesel
					</div>
					<div class="form-group">
						<label>Image</label>
						<form:input path="image" cssClass="form-control" id="image" />
					</div>
					
					<div class="form-group">
					<h4>
						<label>Seating</label>&nbsp;&nbsp;&nbsp;
						<input type="radio" name="seating" value="2" checked>2&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="4">4&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="3">5&nbsp;&nbsp;&nbsp;
							<input type="radio" name="seating" value="6">6</h4>
					</div>
					<div class="form-group">
						<h4><label>Transmission</label>&nbsp;&nbsp;&nbsp;
						
							<input type="radio" name="transmission" value="Manual" checked>Manual&nbsp;&nbsp;&nbsp;
							<input type="radio" name="transmission" value="Automatic">Automatic</h4>
					</div>
					<div class="form-group">
						<label>Amount per day</label>
						<form:input path="amount" cssClass="form-control" id="amount" />
					</div>
					<div class="form-group">
					<h4>
						     <label>Status</label>
						&nbsp;&nbsp;&nbsp;
							<input type="radio" name="status" value="Available" checked>Available&nbsp;&nbsp;&nbsp;
							<input type="radio" name="status" value="Not Available">Not
							Available
						</h4>

					</div>

					<button type="submit" class="btn btn-primary">
						Save
						

					</button>
				</form:form>
				</h4>
			</c:when>
		</c:choose>


	</div>
</body>
</html>