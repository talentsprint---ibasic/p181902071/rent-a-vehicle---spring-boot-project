<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

	<div role="navigation">
			<div class="navbar navbar-inverse">
				<a href="/welcome" class="navbar-brand"> <text
						style="width:40px;font-family: verdana; color : red;">Rent<i
						style="width: 50px; color: yellow">A<sup>awesome</sup></i> <b
						style="width: 40px; color: blue">Vehicle</b></text>
				</a>

				<div class="navbar-collapse collapse" style="float: right">
					<ul class="nav navbar-nav">
						<li><a>Welcome <%String email = (String)session.getAttribute("email");
						out.print(email); %></a></li>
						<li><a href="/logout">Logout</a></li>

					</ul>
				</div>
			</div>
			</div>
	<div class="container text-center">
					<h3>Booking</h3>
					<hr>
		<form class="form-horizontal" method="POST" action="booked">
						<c:if test="${not empty error}">
							<div class="alert alert-danger">
								<c:out value="${error }"></c:out>
							</div>
						</c:if>

						<div class="form-group">
							<label class="control-label col-md-3">Vid</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="Car"
									value="${book.car.id }" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Vtype</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="vehicletype"
									value="${book.vehicletype.id}" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Start</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="startdate"
									value="${book.startdate}" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">end Id</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="enddate"
									value="${book.enddate}" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">amount</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="amount"
									value="${book.amount }" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">status</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="status"
									value="${book.status }" required />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">userid</label>
							<div class="col-md-7">
								<input type="text" class="form-control" name="userid"
									value="${book.userid.id }" required />
							</div>
						</div>
						<div class="form-group ">
							<input type="submit" class="btn btn-primary" value="Confirm Booking" />
						</div>
					
					</form>
		
			
		</div>
	

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>

</body>
</html>