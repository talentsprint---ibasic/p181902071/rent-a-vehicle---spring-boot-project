<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>User home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/welcome" class="navbar-brand"> <text
					style="width:40px;font-family: verdana; color : red;">Rent<i
					style="width: 50px; color: yellow">A<sup>awesome</sup></i> <b
					style="width: 40px; color: blue">Vehicle</b></text>
			</a>

			<div class="navbar-collapse collapse" style="float: right">
				<ul class="nav navbar-nav">
					<li><a>Welcome <%
						String email = (String) session.getAttribute("email");
						out.print(email);
					%></a></li>
					<li><a href="/logout">Logout</a></li>

				</ul>
			</div>
		</div>
		<div class="container text-center" id="tasksDiv">
			<div class="row">

				<div class="col-md-6">
					<img class="inner-name" src="images/city.jpeg" height=500 width=500 />
				</div>

				<div class="col-md-6">


					<h3 style="font-family: verdana">Select city and vehicle</h3>
					<hr>
					<div class="table-responsive">


						<form action="/subcategory" method="post"
							onsubmit="this.form.submit();">


							<select class="form-control" name="city" id="city">
								<option value="0">Select city</option>
								<c:forEach items="${citylist}" var="cList">
									<option value="${cList.id}">${cList.cityname}</option>
								</c:forEach>

							</select> 
							<br> <select class="form-control" name="vehicle"
								id="vehicle">
								<option value="0">Two wheeler / Three wheeler</option>
								<c:forEach items="${vehiclelist}" var="v">
									<option value="${v.id}">${v.vehiclename}</option>
								</c:forEach>
							</select> <br>
							<br>
							<br> <input class="btn btn-primary" type="submit"
								value="Search" name="Search"> <input
								class="btn btn-primary" type="submit" value="MyPosts"
								name="MyPost">

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>