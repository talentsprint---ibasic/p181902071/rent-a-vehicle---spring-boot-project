<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Create an account</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/welcome" class="navbar-brand"> <text
					style="width:40px;font-family: verdana; color : red;">Rent<i
					style="width: 50px; color: yellow">A<sup>awesome</sup></i> <b
					style="width: 40px; color: blue">Vehicle</b></text>
			</a>

			<div class="navbar-collapse collapse" style="float: right">
				<ul class="nav navbar-nav">
					
						<c:if test="${pageContext.request.userPrincipal.name != null}">
							<form id="logoutForm" method="POST"
								action="${contextPath}/logout">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>

							<h2>
								Welcome ${pageContext.request.userPrincipal.name} | <a
									onclick="document.forms['logoutForm'].submit()">Logout</a>
							</h2>
						</c:if>
						
					
				</ul>
			</div>
		</div>
	</div>
		<div class="container text-center" id="tasksDiv">
			<div class="row">

				<div class="col-md-6">
					<img class="inner-name" src="images/city.jpeg" height=500 width=500 />
				</div>

				<div class="col-md-6">
			<div class="table-responsive">


				<form action="${contextPath}/book">
					<h3>select locality and type</h3>
					<hr>

					<select class="form-control" name="locality" id="locality">
						<option value="0">Select locality</option>
						<c:forEach items="${localitylist}" var="List">
							<option value="${List.localityid}">${List.localityname}</option>
						</c:forEach>
					</select> <br> <select class="form-control" name="type"
						id="type">
						<option value="0">Select Subtype</option>
						<c:forEach items="${typelist}" var="v">
							<option value="${v.subid}">${v.typename}</option>
						</c:forEach>
					</select> <br> <input type="submit" value="GO" class="btn btn-primary" />
				</form>
				
</div>
</div>
			</div>
		</div>
	

</body>
</html>