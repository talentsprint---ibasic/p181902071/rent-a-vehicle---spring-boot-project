package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.model.Vehicle;



public interface SubtypeRepository extends CrudRepository<Subtype, Integer> {
	public List<Subtype> findByTypeid(Vehicle typeid);

	public Subtype findByTypename(String typename);
}
