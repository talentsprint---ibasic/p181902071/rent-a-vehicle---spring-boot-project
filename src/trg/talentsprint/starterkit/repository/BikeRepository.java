package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import trg.talentsprint.starterkit.model.Bike3;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;


public interface BikeRepository extends CrudRepository<Bike3, Integer> {

	List<Bike3> findBySubtypeidAndLocalityid(Subtype id1, Locality id2);
	


}
