package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import trg.talentsprint.starterkit.model.Book1;
import trg.talentsprint.starterkit.model.Car1;
import trg.talentsprint.starterkit.model.User;


public interface BookRepository extends CrudRepository<Book1, Integer> {
	List<Book1> findById(int id);

	List<Book1> findByCarAndUserid(Car1 ca, User user);

	List<Book1> findByCar(Car1 car1);

}
