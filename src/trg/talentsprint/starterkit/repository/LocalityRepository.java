package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import trg.talentsprint.starterkit.model.City;
import trg.talentsprint.starterkit.model.Locality;


public interface LocalityRepository extends CrudRepository<Locality, Integer> {
	List<Locality> findByCityid(City cityid);
	//String getLocalityname(int id);

	Locality findByLocalityname(String localityname);

	
	
}
