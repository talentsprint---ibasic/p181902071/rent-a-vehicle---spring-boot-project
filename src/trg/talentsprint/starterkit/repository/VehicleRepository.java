package trg.talentsprint.starterkit.repository;

import org.springframework.data.repository.CrudRepository;

import trg.talentsprint.starterkit.model.Vehicle;



public interface VehicleRepository extends CrudRepository<Vehicle, Integer> {

	Vehicle findByVehiclename(String vehicle);

}
