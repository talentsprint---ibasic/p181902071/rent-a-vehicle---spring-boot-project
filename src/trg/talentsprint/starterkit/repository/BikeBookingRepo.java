package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


import trg.talentsprint.starterkit.model.Bike3;
import trg.talentsprint.starterkit.model.BikeBooking;
import trg.talentsprint.starterkit.model.User;

public interface BikeBookingRepo extends CrudRepository<BikeBooking, Integer> {
	List<BikeBooking> findById(int id);

	List<BikeBooking> findByBikeAndUserid(Bike3 bike, User userid);

	List<BikeBooking> findByBike(Bike3 bike);
}
