package trg.talentsprint.starterkit.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import trg.talentsprint.starterkit.model.Car1;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.model.User;


public interface Car1Repo extends CrudRepository<Car1, Integer> {

	public List<Car1> findBySubtypeidAndLocalityid(Subtype subtype, Locality locality);

	public List<Car1> findBySubtypeidAndLocalityidAndOwnerid(Subtype subtype, Locality locality, User user);
	
	
	
	
}
