package trg.talentsprint.starterkit.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subtype")
public class Subtype {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int subid;

	@Column(name = "typename")
	private String typename;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Vehicle typeid;

	
	public Subtype() {
		super();
	}

	@OneToMany(mappedBy = "subtypeid")
	private List<Car1> car1;
	
	@OneToMany(mappedBy = "subtypeid")
	private List<Bike3> bike;
	
	public int getSubid() {
		return subid;
	}

	public void setSubid(int subid) {
		this.subid = subid;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public Vehicle getTypeid() {
		return typeid;
	}

	public void setTypeid(Vehicle typeid) {
		this.typeid = typeid;
	}

	public Subtype(String typename, Vehicle typeid) {
		super();
		this.typename = typename;
		this.typeid = typeid;
	}

	
}