package trg.talentsprint.starterkit.model;


import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "vehicletype")
public class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "vehiclename")
	private String vehiclename;

	public Vehicle(String vehiclename) {
		super();
		this.vehiclename = vehiclename;
		
	}

	public Vehicle() {
		super();
	}

	@OneToMany(mappedBy = "typeid")
	private List<Subtype> subtype;
	@OneToMany(mappedBy = "vehicletype")
	private List<Book1> book;
	@OneToMany(mappedBy = "vehicletype")
	private List<BikeBooking> bikebooking;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVehiclename() {
		return vehiclename;
	}

	public void setVehiclename(String vehiclename) {
		this.vehiclename = vehiclename;
	}

}
