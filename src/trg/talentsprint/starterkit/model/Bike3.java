package trg.talentsprint.starterkit.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bike3")
public class Bike3 {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bikeid;
	private String bikename;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Subtype subtypeid;
		private String fuel;
	private int seating;
	private String transmission;
	private String colour;
	private int YOP;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private User ownerid;
	
	@OneToMany(mappedBy = "bike")
	private List<BikeBooking> bikebooking;
	
	
	public List<BikeBooking> getBikebooking() {
		return bikebooking;
	}
	public void setBikebooking(List<BikeBooking> bikebooking) {
		this.bikebooking = bikebooking;
	}
	public Bike3() {
		super();
	}
	public Bike3(String bikename, Subtype subtypeid, String fuel, int seating, String transmission, String colour,
			int yOP, User ownerid, String mileage, String displacement, String bhp, Locality localityid, String image,
			String status, int amount) {
		super();
		this.bikename = bikename;
		this.subtypeid = subtypeid;
		this.fuel = fuel;
		this.seating = seating;
		this.transmission = transmission;
		this.colour = colour;
		YOP = yOP;
		this.ownerid = ownerid;
		this.mileage = mileage;
		this.displacement = displacement;
		this.bhp = bhp;
		this.localityid = localityid;
		this.image = image;
		this.status = status;
		this.amount = amount;
	}
	private String mileage;
	private String displacement;
	private String bhp;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Locality localityid;
		private String image;
	private String status;
	private int amount;
	
	public int getBikeid() {
		return bikeid;
	}
	public void setBikeid(int bikeid) {
		this.bikeid = bikeid;
	}
	public String getBikename() {
		return bikename;
	}
	public void setBikename(String bikename) {
		this.bikename = bikename;
	}
	
	public Subtype getSubtypeid() {
		return subtypeid;
	}
	public void setSubtypeid(Subtype subtypeid) {
		this.subtypeid = subtypeid;
	}
	public Locality getLocalityid() {
		return localityid;
	}
	public void setLocalityid(Locality localityid) {
		this.localityid = localityid;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public int getSeating() {
		return seating;
	}
	public void setSeating(int seating) {
		this.seating = seating;
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public int getYOP() {
		return YOP;
	}
	public void setYOP(int yOP) {
		YOP = yOP;
	}
	public User getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(User ownerid) {
		this.ownerid = ownerid;
	}
	public String getMileage() {
		return mileage;
	}
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	public String getDisplacement() {
		return displacement;
	}
	public void setDisplacement(String displacement) {
		this.displacement = displacement;
	}
	public String getBhp() {
		return bhp;
	}
	public void setBhp(String bhp) {
		this.bhp = bhp;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	

}
