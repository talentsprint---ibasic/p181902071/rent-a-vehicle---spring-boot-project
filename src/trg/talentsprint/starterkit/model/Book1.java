package trg.talentsprint.starterkit.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "carbooking")
public class Book1 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Car1 car;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Vehicle vehicletype;
	private Date startdate;
	private Date enddate;
	private int amount;
	private String status;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private User userid;
	
	public Book1() {
		
	}
	
	
	public Book1(Car1 vehicleid, Vehicle vehicletype, Date startdate, Date enddate, int amount, String status,
			User userid) {
		super();
		this.car = vehicleid;
		this.vehicletype = vehicletype;
		this.startdate = startdate;
		this.enddate = enddate;
		this.amount = amount;
		this.status = status;
		this.userid = userid;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Vehicle getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(Vehicle vehicletype) {
		this.vehicletype = vehicletype;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public User getUserid() {
		return userid;
	}
	public void setUserid(User userid) {
		this.userid = userid;
	}


	public Car1 getCar() {
		return car;
	}


	public void setCar(Car1 car) {
		this.car = car;
	}
	
	@Override
	public String toString() {
		return "Book1 [id=" + id + ", vehicleid=" + car + ", vehicletype=" + vehicletype + ", startdate="
				+ startdate + ", enddate=" + enddate + ", amount=" + amount + ", status=" + status + ", userid="
				+ userid + "]";
	}


	
}
