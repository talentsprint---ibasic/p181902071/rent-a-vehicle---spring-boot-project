package trg.talentsprint.starterkit.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "locality")
public class Locality {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int localityid;

	@Column(name = "localityname")
	private String localityname;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private City cityid;

	@OneToMany(mappedBy = "localityid")
	private List<Car1> car1;

	@OneToMany(mappedBy = "localityid")
	private List<Bike3> bike;

	public Locality() {

	}

	public Locality(String localityname, City cityid) {
		super();
		this.localityname = localityname;
		this.cityid = cityid;
	}

	public String getLocalityname() {
		return localityname;
	}

	public void setLocalityame(String localityname) {
		this.localityname = localityname;
	}

	public int getLocalityid() {
		return localityid;
	}

	public void setLocalityid(int localityid) {
		this.localityid = localityid;
	}

	public City getCityid() {
		return cityid;
	}

	public void setCityid(City cityid) {
		this.cityid = cityid;
	}

	public void setLocalityname(String localityname) {
		this.localityname = localityname;
	}

}
