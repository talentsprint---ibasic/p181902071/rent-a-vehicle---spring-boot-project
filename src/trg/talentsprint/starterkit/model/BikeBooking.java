package trg.talentsprint.starterkit.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bikebooking")
public class BikeBooking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Bike3 bike;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Vehicle vehicletype;
	private Date startdate;
	private Date enddate;
	private int amount;
	private String status;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private User userid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Bike3 getBike() {
		return bike;
	}

	public void setBike(Bike3 bike) {
		this.bike = bike;
	}

	public Vehicle getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(Vehicle vehicletype) {
		this.vehicletype = vehicletype;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUserid() {
		return userid;
	}

	public void setUserid(User userid) {
		this.userid = userid;
	}

	public BikeBooking() {
		super();
	}

	public BikeBooking(Bike3 bike, Vehicle vehicletype, Date startdate, Date enddate, int amount, String status,
			User userid) {
		super();
		this.bike = bike;
		this.vehicletype = vehicletype;
		this.startdate = startdate;
		this.enddate = enddate;
		this.amount = amount;
		this.status = status;
		this.userid = userid;
	}

}
