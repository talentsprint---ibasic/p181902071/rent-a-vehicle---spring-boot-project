package trg.talentsprint.starterkit.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "car2")
public class Car1 {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String carname;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Subtype subtypeid;
	

	private String fuel;
	private int seating;
	private String transmission;
	private String colour;
	private int YOP;
	private int amount;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private User ownerid;
	private String milealage;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn()
	private Locality localityid;
	
	
	private String status = "Available";
	private String image;
	
	
	@OneToMany(mappedBy = "car")
    private List<Book1> book;
	private String mileage;
	

	public List<Book1> getBook() {
		return book;
	}


	public void setBook(List<Book1> book) {
		this.book = book;
	}


	public Car1() {

	}

	
	public Car1(int id, String carname, Subtype subtypeid, String fuel, int seating, String transmission, String colour,
			int yOP, int amount, User ownerid, String mileage, Locality localityid, String status,String image) {
		super();
		this.id = id;
		this.carname = carname;
		this.subtypeid = subtypeid;
		this.fuel = fuel;
		this.seating = seating;
		this.transmission = transmission;
		this.colour = colour;
		YOP = yOP;
		this.amount = amount;
		this.ownerid = ownerid;
		this.mileage = mileage;
		this.localityid = localityid;
		this.status = status;
		this.image = image;
		
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCarname() {
		return carname;
	}

	public void setCarname(String carname) {
		this.carname = carname;
	}


	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public int getSeating() {
		return seating;
	}

	public void setSeating(int seating) {
		this.seating = seating;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getYOP() {
		return YOP;
	}

	public void setYOP(int yOP) {
		YOP = yOP;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	

	

	public User getOwnerid() {
		return ownerid;
	}


	public void setOwnerid(User ownerid) {
		this.ownerid = ownerid;
	}


	public String getMileage() {
		return mileage;
	}

	public void setMileage(String milealage) {
		this.mileage = mileage;
	}

	
	

	public Subtype getSubtypeid() {
		return subtypeid;
	}


	public void setSubtypeid(Subtype subtypeid) {
		this.subtypeid = subtypeid;
	}


	public Locality getLocalityid() {
		return localityid;
	}


	public void setLocalityid(Locality localityid) {
		this.localityid = localityid;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}

	
	
}
