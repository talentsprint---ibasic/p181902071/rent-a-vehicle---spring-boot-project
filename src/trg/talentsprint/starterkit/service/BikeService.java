package trg.talentsprint.starterkit.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Bike3;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.repository.BikeRepository;


@Service
@Transactional
public class BikeService {
	@Autowired
	BikeRepository bike;
	
	public List<Bike3> findBike(Subtype id1,Locality id2){
		return bike.findBySubtypeidAndLocalityid(id1, id2);
	}
	
	public Bike3 getBike(int id) {
		return bike.findById(id).get();
	}

	public List<Bike3> findBySubtypeidAndLocalityid(Subtype subtype, Locality locality) {
		// TODO Auto-generated method stub
		return bike.findBySubtypeidAndLocalityid(subtype, locality);
	}

	public void saveBike(Bike3 b) {
		// TODO Auto-generated method stub
		bike.save(b);
	}

	public void deleteBike(int id) {
		// TODO Auto-generated method stub
		bike.deleteById(id);
	}

	

	

	

}
