package trg.talentsprint.starterkit.service;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Bike3;
import trg.talentsprint.starterkit.model.BikeBooking;
import trg.talentsprint.starterkit.model.Book1;
import trg.talentsprint.starterkit.model.Car1;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.repository.BikeBookingRepo;
import trg.talentsprint.starterkit.repository.BookRepository;


@Service
@Transactional
public class BookService {

	@Autowired
	BookRepository bookRepo;
	
	@Autowired
	BikeBookingRepo bikeBook;
	
	public BookService(BookRepository bookRepo) {
		this.bookRepo = bookRepo;
	}
	
	public void saveCar(Book1 book) {
		bookRepo.save(book);
	}

	public List<Book1> finById(int id) {
		// TODO Auto-generated method stub
		return bookRepo.findById(id);
	}

	public void saveBike(BikeBooking book) {
		// TODO Auto-generated method stub
		bikeBook.save(book);
	}
	public List<BikeBooking> finByBikeId(int id) {
		// TODO Auto-generated method stub
		return bikeBook.findById(id);
	}

	public List<Book1> findByCarAndUserid(Car1 ca, User user) {
		// TODO Auto-generated method stub
		return bookRepo.findByCarAndUserid(ca,user);
	}

	public List<BikeBooking> findByBikeAndUserid(Bike3 bike, User userid) {
		// TODO Auto-generated method stub
		return bikeBook.findByBikeAndUserid(bike,userid);
	}
	public List<BikeBooking> findByBike(Bike3 bike) {
		// TODO Auto-generated method stub
		return bikeBook.findByBike(bike);
	}

	public List<Book1> finByCarId(Car1 car1) {
		// TODO Auto-generated method stub
		return bookRepo.findByCar(car1);
	}

	
	
	
	
	
}
