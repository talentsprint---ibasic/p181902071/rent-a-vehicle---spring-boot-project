package trg.talentsprint.starterkit.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trg.talentsprint.starterkit.model.City;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.model.Vehicle;
import trg.talentsprint.starterkit.repository.LocalityRepository;
import trg.talentsprint.starterkit.repository.SubtypeRepository;


@Service
@Transactional
public class SubcategoryService {

	@Autowired
	LocalityRepository localRepo;

	public List<Locality> getLocalities(City id) {
		return localRepo.findByCityid(id);
	}

	public Locality findByLocalityId(int id) {
		return localRepo.findById(id).get();
	}
	

	@Autowired
	SubtypeRepository subtyperepo;

	public List<Subtype> findByTypeid(Vehicle typeid) {
		return subtyperepo.findByTypeid(typeid);
	}

	public List<Subtype> showSubtype() {
		List<Subtype> users = new ArrayList<Subtype>();
		for (Subtype user : subtyperepo.findAll()) {
			users.add(user);
		}

		return users;
	}

	public Subtype findBySubId(int id) {
		return subtyperepo.findById(id).get();
	}

	public Locality findByLocalityname(String localityname) {
		// TODO Auto-generated method stub
		return localRepo.findByLocalityname(localityname);
	}

	public Subtype findByTypename(String typename) {
		// TODO Auto-generated method stub
		return subtyperepo.findByTypename(typename);
	}

	
}
