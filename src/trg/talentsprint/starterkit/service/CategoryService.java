package trg.talentsprint.starterkit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trg.talentsprint.starterkit.model.City;
import trg.talentsprint.starterkit.model.Vehicle;
import trg.talentsprint.starterkit.repository.CityRepository;
import trg.talentsprint.starterkit.repository.VehicleRepository;





@Service
@Transactional
public class CategoryService {
	 @Autowired
	 CityRepository cityRepo;
	
	public List<City> showAllCity(){
		List<City> users = new ArrayList<City>();
		for(City user : cityRepo.findAll()) {
			users.add(user);
		}
		
		return users;
	}
	public List<City> findByCityId(int id) {
		List<City> users = new ArrayList<City>();
		for(City user : cityRepo.findAllById(id)) {
			users.add(user);
		}
		
		  return users;
		 }
	
	public String getCityName() {
		  return new City().getCityname();
		 }
	public City findById(int id) {
		return cityRepo.findById(id).get();
	}
	@Autowired
	 VehicleRepository vehicleRepo;
	public List<Vehicle> vehicleType(){
		List<Vehicle> users = new ArrayList<Vehicle>();
		for(Vehicle user : vehicleRepo.findAll()) {
			users.add(user);
		}
		
		return users;
	}
	public Vehicle findByVId(int id) {
		return vehicleRepo.findById(id).get();
	}
	public City findByCityname(String city) {
		// TODO Auto-generated method stub
		return cityRepo.findByCityname(city);
	}
	public Vehicle findByVehiclename(String vehicle) {
		// TODO Auto-generated method stub
		return vehicleRepo.findByVehiclename(vehicle);
	}
	
}
