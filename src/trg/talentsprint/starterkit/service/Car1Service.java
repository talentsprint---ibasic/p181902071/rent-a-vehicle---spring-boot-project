package trg.talentsprint.starterkit.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

import trg.talentsprint.starterkit.model.Car1;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.repository.Car1Repo;


@Service
@Transactional
public class Car1Service {

	@Autowired
	Car1Repo car;

	public List<Car1> findBySubtypeidAndLocalityid(Subtype subtype,Locality locality) {

		return car.findBySubtypeidAndLocalityid(subtype,locality);
	}

	public Car1 getCar(int id) {

		return car.findById(id).get();
	}

	public void deleteCar(int id) {
		// TODO Auto-generated method stub
		car.deleteById(id);
	}

	public void save(Car1 car2) {
		// TODO Auto-generated method stub
		car.save(car2);
	}

	public List<Car1> findBySubtypeidAndLocalityidAndUserid(Subtype subtype, Locality locality, User user) {
		// TODO Auto-generated method stub
		return car.findBySubtypeidAndLocalityidAndOwnerid(subtype,locality,user);
	}
	
}
