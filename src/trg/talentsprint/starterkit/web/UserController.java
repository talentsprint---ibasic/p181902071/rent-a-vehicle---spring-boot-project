package trg.talentsprint.starterkit.web;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import trg.talentsprint.starterkit.model.Bike3;
import trg.talentsprint.starterkit.model.BikeBooking;
import trg.talentsprint.starterkit.model.Book1;
import trg.talentsprint.starterkit.model.Car1;
import trg.talentsprint.starterkit.model.City;
import trg.talentsprint.starterkit.model.Locality;
import trg.talentsprint.starterkit.model.Subtype;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.model.Vehicle;
import trg.talentsprint.starterkit.service.BikeService;
import trg.talentsprint.starterkit.service.BookService;
import trg.talentsprint.starterkit.service.Car1Service;
import trg.talentsprint.starterkit.service.CategoryService;
import trg.talentsprint.starterkit.service.SecurityService;
import trg.talentsprint.starterkit.service.SubcategoryService;
import trg.talentsprint.starterkit.service.UserService;
import trg.talentsprint.starterkit.validator.UserValidator;

@Controller
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator userValidator;
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private SubcategoryService subcategoryService;

	@Autowired
	private BikeService bikeService;

	@Autowired
	private Car1Service carService;

	@Autowired
	private BookService bookService;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "registration";
	}

	@PostMapping("/registration")
	public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
		userValidator.validate(userForm, bindingResult);

		if (bindingResult.hasErrors()) {
			return "registration";
		}

		userService.save(userForm);

		securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

		return "redirect:/welcome";
	}

	@GetMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "login";
	}

	@GetMapping({ "/", "/welcome" })
	public String welcome(Model model) {
		List<City> c = categoryService.showAllCity();
		List<Vehicle> l = categoryService.vehicleType();

		model.addAttribute("citylist", c);
		model.addAttribute("vehiclelist", l);

		return "welcome";
	}

	@GetMapping(value = "/subcategory", params = "Search")
	public String subCategory(HttpServletRequest request, ModelMap model, HttpSession session) {

		int id = Integer.parseInt(request.getParameter("city"));
		int id1 = Integer.parseInt(request.getParameter("vehicle"));
		City c = categoryService.findById(id);

		Vehicle v = categoryService.findByVId(id1);

		List<Locality> l1 = subcategoryService.getLocalities(c);

		model.addAttribute("localitylist", l1);

		List<Subtype> l2 = subcategoryService.findByTypeid(v);
		model.addAttribute("typelist", l2);

		return "subcategory";
	}

	@GetMapping(value = "/book")
	public String book(HttpServletRequest request, ModelMap model, HttpSession session) {
		int id1 = Integer.parseInt(request.getParameter("type"));
		int id2 = Integer.parseInt(request.getParameter("locality"));

		Locality locality = subcategoryService.findByLocalityId(id2);
		String localityname = locality.getLocalityname();
		Subtype subtype = subcategoryService.findBySubId(id1);
		String typename = subtype.getTypename();

		String u = request.getUserPrincipal().getName();
		User user = userService.findByUsername(u);

		if (subtype.getTypeid().getId() == 1) {
			List<Car1> car = carService.findBySubtypeidAndLocalityid(subtype, locality);
			System.out.println("hii");
			List<Car1> ca = new ArrayList<Car1>();
			for (Car1 car1 : car) {

				if (user.getId() != car1.getOwnerid().getId()) {
					ca.add(car1);

				}
			}

			model.addAttribute("car", ca);

		} else {

			List<Bike3> bike = bikeService.findBike(subtype, locality);
			List<Bike3> b = new ArrayList<Bike3>();
			for (Bike3 bike3 : bike) {

				if (user.getId() != bike3.getOwnerid().getId()) {
					b.add(bike3);
				}
			}

			model.addAttribute("bike", b);

		}
		session.setAttribute("localityname", localityname);
		session.setAttribute("typename", typename);

		return "book";
	}

	@GetMapping(value = "/carbooking")
	public String carbooking(@RequestParam("date1") Date d1, @RequestParam("date2") Date d2, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		int id = Integer.parseInt(request.getParameter("bid"));
		Car1 ca = carService.getCar(id);

		String u = request.getUserPrincipal().getName();
		User user = userService.findByUsername(u);
		System.out.println(u);
		String localityname = (String) session.getAttribute("localityname");
		Locality locality = subcategoryService.findByLocalityname(localityname);
		String typename = (String) session.getAttribute("typename");
		Subtype subtype = subcategoryService.findByTypename(typename);

		double timeDiff = d2.getTime() - d1.getTime();
		int daysDiff = (int) (timeDiff / (1000 * 3600 * 24));
		model.addAttribute("days", daysDiff);
		Book1 book = new Book1();
		if (ca.getStatus().equals("Not Available")) {
			model.addAttribute("car", ca);
			model.addAttribute("locality", locality);
			model.addAttribute("subtype", subtype);
			return "notavailable";
		} else {

			book.setStartdate(d1);
			book.setEnddate(d2);
			book.setUserid(user);
			// book.setVehicletype(v);
			book.setCar(ca);
			book.setStatus("Booked");
			book.setAmount(ca.getAmount() * daysDiff);
			model.addAttribute("book", book);

		}

		model.addAttribute("locality", locality);
		model.addAttribute("subtype", subtype);
		return "booking";
	}

	@GetMapping("/booked")
	public String booked(ModelMap model, @ModelAttribute Book1 book, BindingResult bindingResult,
			HttpServletRequest request) {

		List<Book1> bookList = bookService.findByCarAndUserid(book.getCar(), book.getUserid());
		System.out.println(bookList);
		if (bookList.isEmpty()) {
			bookService.saveCar(book);
		} else {
			for (Book1 book1 : bookList) {
				if (book1.getCar().getId() == book.getCar().getId()
						|| book1.getUserid().getId() == book.getUserid().getId()) {

					request.setAttribute("error", "You have already booked this car");

					System.out.println("bye");
					return "booking";

				}
			}
		}
		model.addAttribute("book", book);

		return "booked";
	}

	@RequestMapping(value = "/bikebooking")
	public String bikebooking(@RequestParam("date1") Date d1, @RequestParam("date2") Date d2,
			HttpServletRequest request, HttpSession session, ModelMap model) {
		System.out.println("hiii");
		int id = Integer.parseInt(request.getParameter("bid"));
		Bike3 bike = bikeService.getBike(id);

		String u = request.getUserPrincipal().getName();
		User user = userService.findByUsername(u);
		String city = (String) session.getAttribute("cityname");
		City c = categoryService.findByCityname(city);

		String vehicle = (String) session.getAttribute("vehiclename");
		Vehicle v = categoryService.findByVehiclename(vehicle);

		String localityname = (String) session.getAttribute("localityname");
		Locality locality = subcategoryService.findByLocalityname(localityname);
		String typename = (String) session.getAttribute("typename");
		Subtype subtype = subcategoryService.findByTypename(typename);

		double timeDiff = d2.getTime() - d1.getTime();
		int daysDiff = (int) (timeDiff / (1000 * 3600 * 24));
		model.addAttribute("days", daysDiff);
		BikeBooking book = new BikeBooking();
		if (bike.getStatus().equals("Not Available")) {
			model.addAttribute("car", bike);
			model.addAttribute("locality", locality);
			model.addAttribute("subtype", subtype);
			return "notavailable";
		} else {

			book.setStartdate(d1);
			book.setEnddate(d2);
			book.setUserid(user);
			book.setVehicletype(v);
			book.setBike(bike);
			book.setAmount(bike.getAmount() * daysDiff);
			book.setStatus("Booked");
			model.addAttribute("book", book);

		}

		model.addAttribute("locality", locality);
		model.addAttribute("subtype", subtype);

		return "bikebooking";
	}

	@GetMapping("/bikebooked")
	public String bikeBooked(ModelMap model, @ModelAttribute BikeBooking book, BindingResult bindingResult,
			HttpServletRequest request) {
		model.addAttribute("book", book);

		List<BikeBooking> bookList = bookService.findByBikeAndUserid(book.getBike(), book.getUserid());
		if (bookList.isEmpty()) {
			bookService.saveBike(book);
		} else {
			for (BikeBooking book1 : bookList) {
				System.out.println("hi");
				if (book1.getBike().getBikeid() == book.getBike().getBikeid()
						|| book1.getUserid().getId() == book.getUserid().getId()) {

					request.setAttribute("error", "You have already booked this bike");

					return "bikebooking";

				}

			}
		}
		return "bikebooked";
	}

	@GetMapping(value = "/subcategory", params = "MyPost")
	public String myPost(HttpServletRequest request, ModelMap model, HttpSession session) {
		int id = Integer.parseInt(request.getParameter("city"));
		int id1 = Integer.parseInt(request.getParameter("vehicle"));
		System.out.println("jhkj");
		City c = categoryService.findById(id);
		String cityname = c.getCityname();

		Vehicle v = categoryService.findByVId(id1);
		String vehiclename = v.getVehiclename();

		String u = request.getUserPrincipal().getName();
		User user = userService.findByUsername(u);
		List<Locality> l1 = subcategoryService.getLocalities(c);

		List<Subtype> l2 = subcategoryService.findByTypeid(v);

		if (v.getId() == 1) {
			List<Car1> car = new ArrayList<Car1>();
			List<Car1> ca = new ArrayList<Car1>();
			for (Locality locality : l1) {

				for (Subtype subtype : l2) {

					car = carService.findBySubtypeidAndLocalityidAndUserid(subtype, locality, user);
					for (Car1 car1 : car) {

						ca.add(car1);

					}
				}
				request.setAttribute("mode", "CAR");

			}
			/*
			 * List<Book1> book = new ArrayList<Book1>(); for(Car1 car2 : ca) {
			 * book.add(bookService.findByCarid(car2)); } model.addAttribute("book", book);
			 */
			model.addAttribute("car", ca);
		} else {
			List<Bike3> bikeList = new ArrayList<Bike3>();
			for (Locality locality : l1) {

				for (Subtype subtype : l2) {

					List<Bike3> bikeListing = bikeService.findBySubtypeidAndLocalityid(subtype, locality);
					for (Bike3 bike : bikeListing) {
						if (user.getId() == bike.getOwnerid().getId()) {

							bikeList.add(bike);
						}
					}

				}

			}
			request.setAttribute("mode", "BIKE");
			model.addAttribute("bikeList", bikeList);
		}

		model.addAttribute("locality", l1);
		model.addAttribute("subtype", l2);
		return "mypost";
	}

	@PostMapping(value = "/interest")
	public String updateInterest(HttpServletRequest request, ModelMap model, HttpSession session) {
		int bikeId = Integer.parseInt(request.getParameter("bikeid").trim());

		Bike3 b = bikeService.getBike(bikeId);
		List<BikeBooking> book = bookService.findByBike(b);
		for (BikeBooking bikeBooking : book) {
			if (bikeBooking.getStatus().equals("Booked")) {
				b.setStatus("Not Available");
				model.addAttribute("b", bikeBooking);

				bikeService.saveBike(b);
			} else if (bikeBooking.getStatus().equals("completed")) {
				b.setStatus("Available");

				model.addAttribute("b", bikeBooking);

				bikeService.saveBike(b);
			}
		}

		model.addAttribute("bike", b);

		return "update";
	}

	@PostMapping(value = "/carinterest")
	public String updateInterestCar(HttpServletRequest request, ModelMap model, HttpSession session) {
		int car = Integer.parseInt(request.getParameter("carid").trim());

		Car1 car1 = carService.getCar(car);
		List<Book1> bookList = bookService.finByCarId(car1);
		for (Book1 book : bookList) {
			if (book.getStatus().equals("Booked")) {
				car1.setStatus("Not Available");
				model.addAttribute("b", book);

				carService.save(car1);
			} else if (book.getStatus().equals("completed")) {
				car1.setStatus("Available");

				model.addAttribute("b", book);

				carService.save(car1);
			}
		}

		model.addAttribute("bike", car1);

		return "update";
	}

	@RequestMapping(value = "/postad/", method = RequestMethod.GET)
	public ModelAndView addPost(HttpServletRequest request, HttpSession session) {
		ModelAndView model = new ModelAndView();

		String u = request.getUserPrincipal().getName();
		User user = userService.findByUsername(u);
		int id1 = Integer.parseInt(request.getParameter("type"));
		int id2 = Integer.parseInt(request.getParameter("locality"));
		Locality locality = subcategoryService.findByLocalityId(id2);
		Subtype subtype = subcategoryService.findBySubId(id1);
		if (subtype.getTypeid().getId() == 2) {
			Bike3 bike = new Bike3();
			bike.setOwnerid(user);
			bike.setLocalityid(locality);
			bike.setSubtypeid(subtype);
			model.addObject("postad", bike);
			model.setViewName("postad");
			request.setAttribute("mode", "BIKEPOST");
		} else {
			Car1 car = new Car1();
			car.setLocalityid(locality);
			car.setSubtypeid(subtype);
			car.setOwnerid(user);
			model.addObject("postad", car);
			model.setViewName("postad");
			request.setAttribute("mode", "CARPOST");
		}

		return model;
	}

	@RequestMapping(value = "/saveBike", method = RequestMethod.POST)
	public String saveBike(@ModelAttribute("postad") Bike3 bike, ModelMap model) {
		bikeService.saveBike(bike);
		return "success";
	}

	@RequestMapping(value = "/addCar", method = RequestMethod.POST)
	public String saveCar(@ModelAttribute("postad") Car1 car, ModelMap model) {
		carService.save(car);
		return "success";
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView editBike(@PathVariable int id, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		Bike3 bike = bikeService.getBike(id);
		model.addObject("postad", bike);
		model.setViewName("postad");
		request.setAttribute("mode", "BIKEPOST");

		return model;
	}

	@RequestMapping(value = "/updateCar/{id}", method = RequestMethod.GET)
	public ModelAndView editCar(@PathVariable int id, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		Car1 car = carService.getCar(id);
		model.addObject("postad", car);
		model.setViewName("postad");
		request.setAttribute("mode", "CARPOST");

		return model;
	}
}
